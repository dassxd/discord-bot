
module.exports = {
    name: 'error',
    distubeEvent: true,
    run(channel, error) {
        console.log(`An error event was sent by DisTube: ${error}`);
    },
};