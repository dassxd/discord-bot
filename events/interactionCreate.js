const { Events } = require('discord.js');


module.exports = {
    name: Events.InteractionCreate,
    async run(interaction) {
        if (!interaction.isChatInputCommand()) return;

        const cmd = interaction.client.commands.get(interaction.commandName);
        if (!cmd) return;
        if (cmd.inVoiceChannel) {
            if (!interaction.member.voice.channel) {
                return await interaction.reply({ content: 'Necesitas estar en un canal de voz para ejecutar este comando.', ephemeral: true });
            }
            else if (interaction.client.voice.adapters.get(interaction.guildId)) {
                if (!interaction.member.voice.channel.members.get(interaction.applicationId)) {
                    return await interaction.reply({ content: 'Necesitas estar en el mismo canal de voz para ejecutar este comando.', ephemeral: true });
                }
            }
        }

        try {
            cmd.run(interaction);
        }
        catch (error) {
            console.error(error);
            if (interaction.replied || interaction.deferred) {
                await interaction.followUp({ content: 'Se ha producido un error al ejecutar el comando.', ephemeral: true });
            } else {
                await interaction.reply({ content: 'Se ha producido un error al ejecutar el comando.', ephemeral: true });
            }
        }
    },
};