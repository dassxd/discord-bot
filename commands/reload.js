const { SlashCommandBuilder } = require('discord.js');

module.exports = {
    data: new SlashCommandBuilder()
        .setName('reload')
        .setDescription('Recarga los comandos.')
        .addStringOption(option =>
            option.setName('comando')
                .setDescription('El comando a recargar.')
                .setRequired(true)),
    run: async (interaction) => {
        const commandName = interaction.options.getString('comando', true).toLowerCase();
        const command = interaction.client.commands.get(commandName);

        const embed = interaction.client.embedModel();

        if (!command) {
            embed.error(`No existe un comando con el nombre \`${commandName}\``);
            return interaction.reply({ embeds: [embed], ephemeral: true });
        }

        delete require.cache[require.resolve(`./${command.data.name}.js`)];

        try {
            console.log(`➡ Recargando comando: ${command.data.name}`);
            interaction.client.commands.delete(command.data.name);
            const newCommand = require(`./${command.data.name}.js`);
            interaction.client.commands.set(newCommand.data.name, newCommand);
            console.log(`⬅ El comando \`${newCommand.data.name}\` fue recargado!`);
            embed.success(`El comando \`${newCommand.data.name}\` fue recargado!`);
        } catch (error) {
            console.error(error);
            embed.error(`Hubo un error al recargar el comando \`${command.data.name}\`: \`${error.message}\``);
        }
        await interaction.reply({ embeds: [embed], ephemeral: true });
    },
};