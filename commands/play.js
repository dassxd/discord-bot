const { SlashCommandBuilder } = require('discord.js');

module.exports = {
    data: new SlashCommandBuilder()
        .setName('play')
        .setDescription('Reproduce una canción en un canal de voz.')
        .addStringOption((option) =>
            option
                .setName('canción')
                .setDescription('Nombre de la canción.')
                .setRequired(true)
        ),
    inVoiceChannel: true,
    run: async (interaction) => {
        console.log('✅ play command');
        await interaction.reply('Playing song...');
        const song = interaction.options.getString('canción');
        await interaction.client.distube.play(interaction.member.voice.channel, song);
    },
};
