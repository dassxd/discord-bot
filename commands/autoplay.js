const { SlashCommandBuilder } = require('discord.js');


module.exports = {
    data: new SlashCommandBuilder()
        .setName('autoplay')
        .setDescription('Alterna en el modo auto reproducción.'),
    inVoiceChannel: true,
    run: async (interaction) => {
        const embed = interaction.client.embedModel()

        try {
            const mode = interaction.client.distube.toggleAutoplay(interaction);
            embed.primary(`Estado de auto-reproducción: ${mode ? "`On`" : "`Off`"}`)
        } catch (error) {
            console.error(error);
            embed.error(`Hubo un error al cambiar el estado de auto-reproducción: \`${error.message}\``);
        }

        await interaction.reply({ embeds: [embed], ephemeral: true });
    },
};
