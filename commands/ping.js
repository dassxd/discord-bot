const { SlashCommandBuilder, EmbedBuilder, bold } = require('discord.js');
const wait = require('node:timers/promises').setTimeout;


module.exports = {
	data: new SlashCommandBuilder()
		.setName('ping')
		.setDescription('Devuelve Pong!'),
	run: async (interaction) => {
		
		const embed = new EmbedBuilder()
			.setColor(interaction.client.theme.primary)
			.setDescription(bold('Pong!'));

		await interaction.deferReply();
		await wait(2000);
		await interaction.editReply({ embeds: [embed] });
	},
};