const Discord = require('discord.js');
const { DisTube } = require('distube');
const { SpotifyPlugin } = require('@distube/spotify');
const { SoundCloudPlugin } = require('@distube/soundcloud');
const { YtDlpPlugin } = require('@distube/yt-dlp');
const fs = require('fs');
const path = require('path');
require('dotenv').config();

const client = new Discord.Client({
    intents: [
        Discord.GatewayIntentBits.Guilds,
        Discord.GatewayIntentBits.GuildMessages,
        Discord.GatewayIntentBits.GuildVoiceStates,
        Discord.GatewayIntentBits.MessageContent,
    ],
});
const TOKEN = process.env.DISCORD_BOT_TOKEN || require('./config.json').DISCORD_BOT_TOKEN;

console.log('Initializing...');

client.distube = new DisTube(client, {
    leaveOnStop: false,
    emitNewSongOnly: true,
    emitAddSongWhenCreatingQueue: false,
    emitAddListWhenCreatingQueue: false,
    plugins: [
        new SpotifyPlugin({
            emitEventsAfterFetching: true,
        }),
        new SoundCloudPlugin(),
        new YtDlpPlugin(),
    ],
});
client.emotes = {
    play: '▶️',
    stop: '⏹️',
    queue: '📄',
    success: '☑️',
    repeat: '🔁',
    error: '❌',
    warning: '⚠️',
    info: 'ℹ️',
};
client.theme = {
    primary: [255, 0, 255],  // Magenta
    secondary: [255, 255, 255], // White
    success: [0, 255, 0], // Green
    error: [255, 0, 0], // Red 
    warning: [255, 255, 0], // Yellow
    info: [0, 0, 255], // Blue
};
client.embedModel = (title = null) => new EmbedModel(title);
client.commands = new Discord.Collection();

const commandsPath = path.join(__dirname, 'commands');
const commandFiles = fs.readdirSync(commandsPath).filter(file => file.endsWith('.js'));
for (const file of commandFiles) {
    console.log(`Loading command: ${file}`);
    const filePath = path.join(commandsPath, file);
    const command = require(filePath);
    if ('data' in command && 'run' in command) {
        client.commands.set(command.data.name, command);
    } else {
        console.log(`[WARNING] The command at ${filePath} is missing a required "name" or "run" property.`);
    }
}

const eventsPath = path.join(__dirname, 'events');
const eventFiles = fs.readdirSync(eventsPath).filter(file => file.endsWith('.js'));
for (const file of eventFiles) {
    const filePath = path.join(eventsPath, file);
    const event = require(filePath);
    if (event.distubeEvent) {
        client.distube.on(event.name, (...args) => event.run(client, ...args));
    } else {
        if (event.once) {
            client.once(event.name, (...args) => event.run(...args));
        } else {
            client.on(event.name, (...args) => event.run(...args));
        }
    }
}

if (TOKEN) {
    client.login(TOKEN);
}
else {
    console.log('Token is not set');
    process.exit(1);
}

class EmbedModel extends Discord.EmbedBuilder {
    constructor(title = null) {
        super();
        if (title)
            this.setTitle(Discord.bold(title));
    }

    info(description = null) {
        this.setColor(client.theme.info);
        this.setDescription(description);
        return this;
    }

    success(description = null) {
        this.setColor(client.theme.success);
        this.setDescription(description);
        return this;
    }

    error(description = null) {
        this.setColor(client.theme.error);
        this.setDescription(description);
        return this;
    }

    warning(description = null) {
        this.setColor(client.theme.warning);
        this.setDescription(description);
        return this;
    }

    primary(description = null) {
        this.setColor(client.theme.primary);
        this.setDescription(description);
        return this;
    }

    secondary(description = null) {
        this.setColor(client.theme.secondary);
        this.setDescription(description);
        return this;
    }
}